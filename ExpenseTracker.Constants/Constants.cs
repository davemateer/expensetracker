﻿using System;

namespace ExpenseTracker
{
    public class ExpenseTrackerConstants
    {
        //public const string ExpenseTrackerAPI = "http://localhost:43321";
        public const string ExpenseTrackerAPI = "http://dmexpensetrackerapi.azurewebsites.net/";

        public const string ExpenseTrackerClient = "https://localhost:44306/";
        //public const string ExpenseTrackerClient = "http://dmexpensetracker.azurewebsites.net";

        //public const string IdSrv = "https://localhost:44305/identity";
        public const string IdSrv = "https://dmidsrv.azurewebsites.net/identity";

        public const string IdSrvIssuerUri = "https://expensetrackeridsrv3/embedded";

        public const string IdSrvToken = IdSrv + "/connect/token";
        public const string IdSrvAuthorize = IdSrv + "/connect/authorize";
        public const string IdSrvUserInfo = IdSrv + "/connect/userinfo";

    }
}
