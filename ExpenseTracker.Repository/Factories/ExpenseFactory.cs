﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using ExpenseTracker.Repository.Entities;

namespace ExpenseTracker.Repository.Factories
{
    public class ExpenseFactory
    {
        public ExpenseFactory()
        { }

        // takes an Expense, and returns DTO.Expense
        public DTO.Expense CreateExpense(Expense expense)
        {
            return new DTO.Expense
            {
                Amount = expense.Amount,
                Date = expense.Date,
                Description = expense.Description,
                ExpenseGroupId = expense.ExpenseGroupId,
                Id = expense.Id
            };
        }


        public Expense CreateExpense(DTO.Expense expense)
        {
            return new Expense
            {
                Amount = expense.Amount,
                Date = expense.Date,
                Description = expense.Description,
                ExpenseGroupId = expense.ExpenseGroupId,
                Id = expense.Id
            };
        }

        public object CreateDataShapedObject(Expense expense, List<string> lstOfFields)
        {
            // pass through from entity to DTO
            return CreateDataShapedObject(CreateExpense(expense), lstOfFields);
        }

        public object CreateDataShapedObject(DTO.Expense expense, List<string> lstOfFields)
        {
            if (!lstOfFields.Any())
            {
                // there are not any fields specified, so return them all
                return expense;
            }

            // dynamically create an object at runtime
            var objectToReturn = new ExpandoObject();
            foreach (var field in lstOfFields)
            {
                var fieldValue = expense.GetType()
                    .GetProperty(field, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)
                    .GetValue(expense, null);
                ((IDictionary<string, object>)objectToReturn).Add(field, fieldValue);
            }
            return objectToReturn;
        }
    }
}
