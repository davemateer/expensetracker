﻿using ExpenseTracker.DTO;
using ExpenseTracker.WebClient.Helpers;
using ExpenseTracker.WebClient.Models;
using Marvin.JsonPatch;
using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using Thinktecture.IdentityModel.Mvc;

namespace ExpenseTracker.WebClient.Controllers
{
    public class ExpenseGroupsController : Controller
    {
        [ResourceAuthorize("Read", "ExpenseGroup")]
        public async Task<ActionResult> Index(int? page = 1)
        {
            var client = ExpenseTrackerHttpClient.GetClient();
            var model = new ExpenseGroupsViewModel();
            HttpResponseMessage egsResponse = await client.GetAsync("api/expensegroupstatusses");

            if (egsResponse.IsSuccessStatusCode)
            {
                string egsContent = await egsResponse.Content.ReadAsStringAsync();
                var lstExpenseGroupStatuses = JsonConvert.DeserializeObject<IEnumerable<ExpenseGroupStatus>>(egsContent);
                model.ExpenseGroupStatuses = lstExpenseGroupStatuses;
            }
            else
            {
                return Content("An error occurred.");
            }

            string userId = (this.User.Identity as ClaimsIdentity).FindFirst("unique_user_key").Value;

            HttpResponseMessage response = 
                await client.GetAsync("api/expensegroups?sort=expensegroupstatusid,title&page=" + page + "&pagesize=5&userid="+userId);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();

                var pagingInfo = HeaderParser.FindAndParsePagingInfo(response.Headers);

                var lstExpenseGroups = JsonConvert.DeserializeObject<IEnumerable<ExpenseGroup>>(content);

                var pagedExpenseGroupsList = new StaticPagedList<ExpenseGroup>(lstExpenseGroups,
                    pagingInfo.CurrentPage,
                    pagingInfo.PageSize,
                    pagingInfo.TotalCount);

                model.ExpenseGroups = pagedExpenseGroupsList;
                model.PagingInfo = pagingInfo;
            }
            else
            {
                return Content("An error occurred.");
            }
            return View(model);
        }

        public async Task<ActionResult> Details(int id)
        {
            var client = ExpenseTrackerHttpClient.GetClient();

            HttpResponseMessage response = await client.GetAsync("api/expensegroups/" + id
                                + "?fields=id,description,title,expenses");
            string content = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var model = JsonConvert.DeserializeObject<ExpenseGroup>(content);
                return View(model);
            }

            return Content("An error occurred");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ExpenseGroup expenseGroup)
        {
            try
            {
                // given_name, family_name and unique_user_key (combination of issuer and subject)
                var claimsIdentity = this.User.Identity as ClaimsIdentity;
                var userId = claimsIdentity.FindFirst("unique_user_key").Value;

                var client = ExpenseTrackerHttpClient.GetClient();

                // an expensegroup is created with a status "Open", for the current user
                expenseGroup.ExpenseGroupStatusId = 1;
                //expenseGroup.UserId = @"https://expensetrackeridsrv3/embedded_1";
                expenseGroup.UserId = userId;

                var serializedItemToCreate = JsonConvert.SerializeObject(expenseGroup);

                var response = await client.PostAsync("api/expensegroups",
                    new StringContent(serializedItemToCreate,
                        System.Text.Encoding.Unicode,
                        "application/json"));

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return Content("An error occurred");

            }
            catch (Exception)
            {
                return Content("An error occurred");
            }
        }

        [ResourceAuthorize("Write", "ExpenseGroup")]
        public async Task<ActionResult> Edit(int id)
        {
            var client = ExpenseTrackerHttpClient.GetClient();

            // don't need all the data, so just return the shape we need
            HttpResponseMessage response = await client.GetAsync("api/expensegroups/" + id
                + "?fields=id,title,description");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var model = JsonConvert.DeserializeObject<ExpenseGroup>(content);
                return View(model);
            }
            return Content("An error occurred.");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, ExpenseGroup expenseGroup)
        {
            try
            {
                var client = ExpenseTrackerHttpClient.GetClient();

                JsonPatchDocument<DTO.ExpenseGroup> patchDoc = new JsonPatchDocument<ExpenseGroup>();
                patchDoc.Replace(eg => eg.Title, expenseGroup.Title);
                patchDoc.Replace(eg => eg.Description, expenseGroup.Description);

                var serializedItemToUpdate = JsonConvert.SerializeObject(patchDoc);

                var response = await client.PatchAsync("api/expensegroups/" + id,
                    new StringContent(serializedItemToUpdate, System.Text.Encoding.Unicode, "application/json"));

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return Content("An error occurred");

            }
            catch (Exception)
            {
                return Content("An error occurred");
            }
        }


        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var client = ExpenseTrackerHttpClient.GetClient();

                var response = await client.DeleteAsync("api/expensegroups/" + id);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return Content("An error occurred");

            }
            catch (Exception)
            {
                return Content("An error occurred");
            }
        }
    }
}
