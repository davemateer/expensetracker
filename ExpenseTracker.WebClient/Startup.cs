﻿using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using ExpenseTracker.WebClient.Helpers;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Newtonsoft.Json.Linq;

[assembly: OwinStartup(typeof(ExpenseTracker.WebClient.Startup))]
namespace ExpenseTracker.WebClient
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // resets.. so doesn't do auto mapping of claim types to .NETs
            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();

            AntiForgeryConfig.UniqueClaimTypeIdentifier = "unique_user_key";

            // wire in our own AuthorizationManager
            app.UseResourceAuthorization(new AuthorizationManager());

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "Cookies"
            });

            //openidconnect middleware which only supports the hybrid flow
            app.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
            {
                ClientId = "mvc",
                Authority = ExpenseTrackerConstants.IdSrv,
                // uri of the mvc app eg https://dmexpensetracker.azurewebsites.net
                RedirectUri = ExpenseTrackerConstants.ExpenseTrackerClient,
                SignInAsAuthenticationType = "Cookies",
                ResponseType = "code id_token token",
                // Access token will contain these scopes
                Scope = "openid profile roles expensetrackerapi",
                Notifications = new OpenIdConnectAuthenticationNotifications
                {

                    MessageReceived = async n =>
                    {
                        EndpointAndTokenHelper.DecodeAndWrite(n.ProtocolMessage.IdToken);
                        EndpointAndTokenHelper.DecodeAndWrite(n.ProtocolMessage.AccessToken);
                        //var userInfo = await EndpointAndTokenHelper
                        //    .CallUserInfoEndpoint(n.ProtocolMessage.AccessToken);
                    },

                    SecurityTokenValidated = async n =>
                    {
                        // n, Authentication Ticket {Microsoft.Owin.Security.AuthenticationTicket}
                        // Identity, Claims .. should be 13 claims 
                        var userInfo = await EndpointAndTokenHelper
                            .CallUserInfoEndpoint(n.ProtocolMessage.AccessToken);

                        // creating 2 new claims
                        var givenNameClaim = new Claim(Thinktecture.IdentityModel.Client.JwtClaimTypes.GivenName,
                            userInfo.Value<string>("given_name"));

                        var familyNameClaim = new Claim(JwtRegisteredClaimNames.FamilyName,
                            userInfo.Value<string>("family_name"));

                        var roles = userInfo.Value<JArray>("role").ToList();

                        // should I be using JwtClaimTypes (older?) or JwtRegisteredClaimNames?
                        var newIdentity = new ClaimsIdentity(
                            n.AuthenticationTicket.Identity.AuthenticationType,
                            JwtRegisteredClaimNames.GivenName,
                             Thinktecture.IdentityModel.Client.JwtClaimTypes.Role);

                        newIdentity.AddClaim(givenNameClaim);
                        newIdentity.AddClaim(familyNameClaim);

                        foreach (var role in roles)
                        {
                            newIdentity.AddClaim(new Claim(
                                Thinktecture.IdentityModel.Client.JwtClaimTypes.Role,
                                role.ToString()
                            ));
                        }

                        // need issuer and subject claim so can uniquly identify a user
                        // and thus make antiforgery work
                        var issuerClaim = n.AuthenticationTicket.Identity
                            .FindFirst(Thinktecture.IdentityModel.Client.JwtClaimTypes.Issuer);
                        var subjectClaim = n.AuthenticationTicket.Identity
                            .FindFirst(Thinktecture.IdentityModel.Client.JwtClaimTypes.Subject);
                        
                        newIdentity.AddClaim(new Claim("unique_user_key",issuerClaim.Value + "_" + subjectClaim.Value));
                        newIdentity.AddClaim(new Claim("access_token",n.ProtocolMessage.AccessToken));

                        // replace the original 13 claims with just 3
                        n.AuthenticationTicket = new AuthenticationTicket(
                            newIdentity,
                            n.AuthenticationTicket.Properties);
                    }
                }
            });
        }
    }
}