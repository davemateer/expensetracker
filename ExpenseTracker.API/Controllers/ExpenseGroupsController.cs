﻿using ExpenseTracker.API.Helpers;
using ExpenseTracker.Repository;
using ExpenseTracker.Repository.Entities;
using ExpenseTracker.Repository.Factories;
using Marvin.JsonPatch;
using Newtonsoft.Json;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Routing;

namespace ExpenseTracker.API.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ExpenseGroupsController : ApiController
    {
        IExpenseTrackerRepository _repository;
        ExpenseGroupFactory _expenseGroupFactory = new ExpenseGroupFactory();
        const int maxPageSize = 100;

        public ExpenseGroupsController()
        {
            _repository = new ExpenseTrackerEFRepository(new ExpenseTrackerContext());
        }

        public ExpenseGroupsController(IExpenseTrackerRepository repository)
        {
            _repository = repository;
        }

        [Route("api/expensegroups", Name = "ExpenseGroupsList")]
        public IHttpActionResult Get(string sort = "id", string status = null, string userId = null, string fields = null,
            int page = 1, int pageSize = 100)
        {
            try
            {
                bool includeExpenses = false;
                var lstOfFields = new List<string>();

                // we should include expenses when the fields string contains expenses or expenses.id
                if (fields != null)
                {
                    lstOfFields = fields.ToLower().Split(',').ToList();
                    includeExpenses = lstOfFields.Any(f => f.Contains("expenses"));
                }

                int statusId = -1;
                if (status != null)
                {
                    switch (status.ToLower())
                    {
                        case "open":
                            statusId = 1;
                            break;
                        case "confirmed":
                            statusId = 2;
                            break;
                        case "processed":
                            statusId = 3;
                            break;
                    }
                }

                IQueryable<ExpenseGroup> expenseGroups;
                if (includeExpenses)
                    expenseGroups = _repository.GetExpenseGroupsWithExpenses();
                else
                    expenseGroups = _repository.GetExpenseGroups();

                expenseGroups = expenseGroups
                    .ApplySort(sort)
                    .Where(eg => statusId == -1 || eg.ExpenseGroupStatusId == statusId)
                    .Where(eg => userId == null || eg.UserId == userId);

                if (pageSize > maxPageSize) pageSize = maxPageSize;

                // calculate data for metadata
                var totalCount = expenseGroups.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pageSize);

                // useful helper which gets the route
                var urlHelper = new UrlHelper(Request);
                var preLink = page > 1 ? urlHelper.Link("ExpenseGroupsList",
                        new
                        {
                            page = page - 1,
                            pageSize,
                            sort,
                            fields,
                            status,
                            userId
                        }) : "";
                var nextLink = page < totalPages ? urlHelper.Link("ExpenseGroupsList",
                       new
                       {
                           page = page + 1,
                           pageSize,
                           sort,
                           fields,
                           status,
                           userId
                       }) : "";

                var paginationHeader = new
                {
                    currentPage = page,
                    pageSize,
                    totalCount,
                    totalPages,
                    previousPageLink = preLink,
                    nextPageLink = nextLink
                };

                HttpContext.Current.Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationHeader));

                return Ok(expenseGroups
                    .Skip(pageSize * (page - 1))
                    .Take(pageSize)
                    .ToList()
                    .Select(eg => _expenseGroupFactory.CreateDataShapedObject(eg,lstOfFields))
                    );

            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        public IHttpActionResult Get(int id, string fields = null)
        {
            try
            {
                bool includeExpenses = false;
                List<string> lstOfFields = new List<string>();

                // we should include expenses when the fields-string contains "expenses"
                if (fields != null)
                {
                    lstOfFields = fields.ToLower().Split(',').ToList();
                    includeExpenses = lstOfFields.Any(f => f.Contains("expenses"));
                }


                Repository.Entities.ExpenseGroup expenseGroup;
                if (includeExpenses)
                {
                    expenseGroup = _repository.GetExpenseGroupWithExpenses(id);
                }
                else
                {
                    expenseGroup = _repository.GetExpenseGroup(id);

                }


                if (expenseGroup != null)
                {
                    return Ok(_expenseGroupFactory.CreateDataShapedObject(expenseGroup, lstOfFields));
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [Route("api/expensegroups")]
        [HttpPost]
        public IHttpActionResult Post([FromBody] DTO.ExpenseGroup expenseGroup)
        {
            try
            {
                if (expenseGroup == null)
                {
                    // Request is malformed ie not an expenseGroup DTO - 400 Bad Request
                    return BadRequest();
                }

                var eg = _expenseGroupFactory.CreateExpenseGroup(expenseGroup);
                RepositoryActionResult<ExpenseGroup> result = _repository.InsertExpenseGroup(eg);

                if (result.Status == RepositoryActionStatus.Created)
                {
                    DTO.ExpenseGroup newExpenseGroup = _expenseGroupFactory.CreateExpenseGroup(result.Entity);
                    //return Created(Request.RequestUri + "/" + newExpenseGroup.Id, newExpenseGroup);
                    return Created(Request.RequestUri + newExpenseGroup.Id.ToString(), newExpenseGroup);

                }

                return BadRequest();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // Put is a full update only ie needs all fields
        public IHttpActionResult Put([FromBody] DTO.ExpenseGroup expenseGroup)
        {
            try
            {
                if (expenseGroup == null)
                    return BadRequest();

                // map
                var eg = _expenseGroupFactory.CreateExpenseGroup(expenseGroup);

                var result = _repository.UpdateExpenseGroup(eg);

                if (result.Status == RepositoryActionStatus.Updated)
                {
                    // map to dto
                    var updatedExpenseGroup = _expenseGroupFactory.CreateExpenseGroup(result.Entity);
                    return Ok(updatedExpenseGroup);
                }
                if (result.Status == RepositoryActionStatus.NotFound)
                {
                    return NotFound();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpPatch]
        public IHttpActionResult Patch(int id,
            [FromBody] JsonPatchDocument<DTO.ExpenseGroup> expenseGroupPatchDocument)
        {
            try
            {
                if (expenseGroupPatchDocument == null)
                    return BadRequest();

                var expenseGroup = _repository.GetExpenseGroup(id);
                if (expenseGroup == null)
                    return NotFound();

                // map to DTO
                var eg = _expenseGroupFactory.CreateExpenseGroup(expenseGroup);

                // apply changes from patchDocument to the DTO we just got from our db
                expenseGroupPatchDocument.ApplyTo(eg);

                // map the DTO with applied changes to the entity and update
                var result = _repository.UpdateExpenseGroup(_expenseGroupFactory.CreateExpenseGroup(eg));

                if (result.Status == RepositoryActionStatus.Updated)
                {
                    // map to dto
                    var patchedExpenseGroup = _expenseGroupFactory.CreateExpenseGroup(result.Entity);
                    return Ok(patchedExpenseGroup);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                var result = _repository.DeleteExpenseGroup(id);

                if (result.Status == RepositoryActionStatus.Deleted)
                {
                    return StatusCode(HttpStatusCode.NoContent);
                }
                if (result.Status == RepositoryActionStatus.NotFound)
                {
                    return NotFound();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }
    }
}
