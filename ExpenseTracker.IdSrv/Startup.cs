﻿using ExpenseTracker.IdSrv.Config;
using IdentityServer3.Core;
using IdentityServer3.Core.Configuration;
using Microsoft.Owin;
using Microsoft.Owin.Security.Facebook;
using Newtonsoft.Json.Linq;
using Owin;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

[assembly: OwinStartup(typeof(ExpenseTracker.IdSrv.Startup))]
namespace ExpenseTracker.IdSrv
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Map("/identity", IdSrvApp =>
            {
                IdSrvApp.UseIdentityServer(new IdentityServerOptions
                {
                    SiteName = "Embedded IdentityServer",
                    IssuerUri = ExpenseTrackerConstants.IdSrvIssuerUri,
                    SigningCertificate = LoadCertificate(),

                    //Factory = InMemoryFactory.Create()
                    //https://identityserver.github.io/Documentation/docsv2/configuration/inMemory.html
                    Factory = new IdentityServerServiceFactory()
                        .UseInMemoryUsers(Users.Get())
                        .UseInMemoryClients(Clients.Get())
                        .UseInMemoryScopes(Scopes.Get()),

                    AuthenticationOptions = new AuthenticationOptions
                    {
                        IdentityProviders = ConfigureIdentityProviders,
                    }
                });
            });
        }

        private void ConfigureIdentityProviders(IAppBuilder app, string signInAsType)
        {
            app.UseFacebookAuthentication(new FacebookAuthenticationOptions
            {
                AuthenticationType = "Facebook",
                Caption = "Sign-in with Facebook",
                SignInAsAuthenticationType = signInAsType,
                AppId = "1115610591890864",
                //AppSecret = GetFacebookAppSecretKey(),
                AppSecret = "04fc9336e906efcbf86446c4610786aa",
                // dm added this http://stackoverflow.com/questions/20928939/how-to-get-facebook-first-and-last-name-values-using-asp-net-mvc-5-and-owin
                Provider = new FacebookAuthenticationProvider
                {
                    OnAuthenticated = (context) =>
                    {
                    // facebook API has changed, and harder to get firstname and lastname
                    // split on name - not a good idea.. but works for now.
                    JToken fullName;
                        if (context.User.TryGetValue("name", out fullName))
                        {
                            JToken firstName = fullName.ToString().Split(' ')[0];
                            JToken lastName = fullName.ToString().Split(' ')[1];
                            context.Identity.AddClaim(new System.Security.Claims.Claim(
                                Constants.ClaimTypes.FamilyName,
                                lastName.ToString()));
                            context.Identity.AddClaim(new System.Security.Claims.Claim(
                                Constants.ClaimTypes.GivenName,
                                firstName.ToString()));
                        }

                        context.Identity.AddClaim(new System.Security.Claims.Claim("role", "WebReadUser"));
                        context.Identity.AddClaim(new System.Security.Claims.Claim("role", "WebWriteUser"));

                        context.Identity.AddClaim(new System.Security.Claims.Claim("role", "MobileReadUser"));
                        context.Identity.AddClaim(new System.Security.Claims.Claim("role", "MobileWriteUser"));

                        return Task.FromResult(0);
                    }
                }


            });
        }

        X509Certificate2 LoadCertificate()
        {
            return new X509Certificate2(
                string.Format(@"{0}\Certificates\idsrv3test.pfx", AppDomain.CurrentDomain.BaseDirectory), "idsrv3test");
        }

        //private string GetFacebookAppSecretKey()
        //{
        //    var root = AppDomain.CurrentDomain.BaseDirectory;
        //    return System.IO.File.ReadLines(root + @"/FacebookAppSecretKey.txt").Take(1).First();
        //}
    }
}
