﻿using IdentityServer3.Core.Models;
using System.Collections.Generic;

namespace ExpenseTracker.IdSrv.Config
{
    public static class Scopes
    {
        public static IEnumerable<Scope> Get()
        {
            var scopes = new List<Scope>
                {
                    // identity scope
                    StandardScopes.OpenId,
                    StandardScopes.Profile,
                    new Scope
                    {
                        Enabled = true,
                        Name = "roles",
                        DisplayName = "Roles",
                        Description = "The roles you belong to.",
                        Type = ScopeType.Identity,
                        Claims = new List<ScopeClaim>
                        {
                            new ScopeClaim("role")
                        }
                    },
                    // scope to access a resource eg an API
                    new Scope
                    {
                        Name = "expensetrackerapi",
                        DisplayName = "ExpenseTracker API Scope",
                        Type = ScopeType.Resource,
                        Enabled = true,

                        
                        //Claims = new List<ScopeClaim>
                        //{
                        //    new ScopeClaim("role")
                        //}
                    }
                 };
            return scopes;
        }
    }
}