﻿using IdentityServer3.Core;
using IdentityServer3.Core.Models;
using System.Collections.Generic;

namespace ExpenseTracker.IdSrv.Config
{
    public static class Clients
    {
        public static IEnumerable<Client> Get()
        {
            return new[]
            {
                new Client
                {
                    Enabled = true,
                    ClientName = "ExpenseTracker MVC Client (Hybrid Flow)",
                    ClientId = "mvc",
                    Flow = Flows.Hybrid,
                    RequireConsent = true,
                    RedirectUris = new List<string>
                    {
                        ExpenseTrackerConstants.ExpenseTrackerClient
                    },
                    // http://stackoverflow.com/a/32578920/26086
                    AllowedScopes = new List<string> {
                        Constants.StandardScopes.OpenId,
                        Constants.StandardScopes.Profile,
                        Constants.StandardScopes.Email,
                        Constants.StandardScopes.Roles,
                        "expensetrackerapi"
                    }
                }
            };
        }
    }
}